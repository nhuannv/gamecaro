package caro.scenes;

import caro.common.BaseGameScene;
import caro.game.GameView;

public class GameScene extends BaseGameScene {
	public static void main(String[] args) {
		setGameView(new GameView());
	}
}
