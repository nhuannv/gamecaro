package caro.entities.logic;

import java.io.IOException;
import java.util.Map;

import caro.entities.Board;

public class BoardLogic {
	public static final int WIDTH = 10;
	public static final int HEIGHT = 10;
	public static final int NUM = 5;
	private static Board board;

	public void draw() {
		String[][] viewXO = board.getView();
		System.out.println("Player 1 <X> - Player 2 <O>");
		for (int i = -1; i < HEIGHT * 2; i++) {
			for (int j = 0; j < WIDTH; j++) {
				if (i % 2 == 0)
					System.out.format("| %s ", viewXO[i/2][j]);
				else {
					System.out.print("----");
				}
			}
			if (i % 2 == 0)
				System.out.print("|");
			else {
				System.out.print("-");
			}
			System.out.println();
		}
	}

	public void fillXO(Map<String, Integer> XO, String enter) {
		board.getView()[XO.get("y")][XO.get("x")] = enter;
	}

	public void cls() {
		try {
			new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

	public void setBoard(Board board) {
		BoardLogic.board = board;
	}

	public static boolean exist(Map<String, Integer> XO) {
		return board.getView()[XO.get("y")][XO.get("x")] != " ";
	}

	public String scanBoard(int row, int col) {
		String[][] viewXO = board.getView();
		int[][] rc = { 
			{ 0,-1, 0, 1 },
			{-1, 0, 1, 0 },
			{ 1,-1,-1, 1 },
			{-1,-1, 1, 1 }
		};
		int i = row, j = col, count;
		for (int direction = 0; direction < 4; direction++) {
			count = 0;
			i = row;
			j = col;
			while (i >= 0 && i < viewXO.length && j >= 0 && j < viewXO.length && viewXO[i][j] == viewXO[row][col]) {
				count++;
				if (count == 5) {
					return viewXO[row][col];
				}
				i += rc[direction][0];
				j += rc[direction][1];
			}
			count--;
			i = row;
			j = col;
			while (i >= 0 && i < viewXO.length && j >= 0 && j < viewXO.length && viewXO[i][j] == viewXO[row][col]) {
				count++;
				if (count == 5) {
					return viewXO[row][col];
				}
				i += rc[direction][2];
				j += rc[direction][3];
			}
		}
		return null;
	}
}
