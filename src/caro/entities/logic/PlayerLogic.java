package caro.entities.logic;

import java.util.Map;

import caro.common.PlayerIO;

public class PlayerLogic {
	private static int countTurn = 0;
	
	public static Map<String, Integer> enter() {		
		Map<String, Integer> rs = PlayerIO.enter();
		if(rs != null) {
			countTurn++;
		}
		return rs;
	}

	public static int getCountTurn() {
		return countTurn;
	}
	
}
