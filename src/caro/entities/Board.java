package caro.entities;

public class Board {
	private String[][] view;
	
	public Board(String[][] view) {
		this.view = view;
	}

	public String[][] getView() {
		return view;
	}

	public void setView(String[][] view) {
		this.view = view;
	}
	
}
