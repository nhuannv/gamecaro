package caro.game;

import java.util.Map;

import caro.common.Const;
import caro.entities.Board;
import caro.entities.logic.BoardLogic;
import caro.entities.logic.PlayerLogic;

public class GameView {

	private BoardLogic boardLogic = new BoardLogic();

	public GameView() {
		initView();
	}

	public void initView() {
		boardLogic.setBoard(new Board(Const.BOARD_INIT));
	}

	public void draw() {
		String p = null;
		boardLogic.cls();
		do {
			boardLogic.draw();
			boolean countTurn = PlayerLogic.getCountTurn() % 2 == 0;
			String playerXO = countTurn ? "Player 1" : "Player 2";
			System.out.println(String.format("%s turn...", playerXO));
			Map<String, Integer> rs;
			do {
				rs = PlayerLogic.enter();
				if (rs == null || BoardLogic.exist(rs)) {
					boardLogic.cls();
					boardLogic.draw();
					System.out.println(String.format("%s enter incorrect...", playerXO));
				}
			} while (rs == null || BoardLogic.exist(rs));
			boardLogic.fillXO(rs, countTurn ? "X" : "O");
			p = boardLogic.scanBoard(rs.get("y"), rs.get("x"));
			if("O".equals(p) || "X".equals(p)) {
				boardLogic.cls();
				boardLogic.draw();
				System.out.println(playerXO + " is winner!!!");
				return;
			}
			boardLogic.cls();
		} while (true);

	}
}
