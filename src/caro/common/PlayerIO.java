package caro.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import caro.entities.logic.BoardLogic;

public class PlayerIO {
	private static Scanner scanner = new Scanner(System.in);
	
	public static Map<String, Integer> enter() {
		Map<String, Integer> result = new HashMap<>();
		System.out.print("Nhap x: ");
		result.put("x", scanner.nextInt() - 1);
		System.out.print("Nhap y: ");
		result.put("y", scanner.nextInt() - 1);
		if(result.get("y") >= BoardLogic.HEIGHT || result.get("x") >= BoardLogic.WIDTH
				|| BoardLogic.exist(result)) {
			return null;
		}
		return result;
	}
}
