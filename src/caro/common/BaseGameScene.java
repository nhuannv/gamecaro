package caro.common;

import caro.game.GameView;

public abstract class BaseGameScene {
	public static void setGameView(GameView gameView) {
		gameView.draw();
	}
}
